/*
 * Public API Surface of dr-lib
 */

export * from './lib/dr-lib.module';
export * from './lib/dynamic-registration/dynamic-registration.service';
export * from './lib/some-component/some-component.component';
export * from './lib/some-service/some-service.service';
export * from './lib/dynamic-registration/dynamic-init.service';
export * from './lib/dynamic-registration/dynamic.component';
