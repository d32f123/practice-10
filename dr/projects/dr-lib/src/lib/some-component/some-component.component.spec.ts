import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SomeComponentComponent } from './some-component.component';
import {DynamicRegistrationService, SomeServiceService} from 'dr-lib';

describe('SomeComponentComponent', () => {
  let component: SomeComponentComponent;
  let fixture: ComponentFixture<SomeComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SomeComponentComponent ],
      providers: [{
        provide: DynamicRegistrationService,
        useValue: {
          resolve: () => new SomeServiceService()
        }
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SomeComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should resolve service', () => {
    expect(component.someService).toBeTruthy();
  });
});
