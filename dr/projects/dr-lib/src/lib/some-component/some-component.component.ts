import { Component, OnInit } from '@angular/core';
import {DynamicRegistrationService, SomeServiceService} from 'dr-lib';

@Component({
  selector: 'lib-some-component',
  templateUrl: './some-component.component.html',
  styleUrls: ['./some-component.component.css']
})
export class SomeComponentComponent implements OnInit {

  public someService: SomeServiceService;

  constructor(dynamicRegistrationService: DynamicRegistrationService) {
    this.someService = dynamicRegistrationService.resolve(SomeServiceService);
  }

  ngOnInit() {
  }

}
