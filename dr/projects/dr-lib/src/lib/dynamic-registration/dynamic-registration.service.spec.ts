import {async, TestBed} from '@angular/core/testing';
import {DynamicRegistrationService} from './dynamic-registration.service';
import {SomeServiceService} from '../some-service/some-service.service';
import {InjectionToken, INJECTOR, Injector} from '@angular/core';

describe('DynamicRegistrationService', () => {
  let service: DynamicRegistrationService;
  let injector: Injector;
  const sampleInjectToken = new InjectionToken('someService');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{
        provide: SomeServiceService,
        useValue: {getValue: () => 'wtf'}
      }, {
        provide: sampleInjectToken,
        useValue: {getValue: () => 'wtf2'}
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(DynamicRegistrationService);
    injector = TestBed.get(INJECTOR);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should register new service', () => {
    service.register(SomeServiceService, {
      injector,
      token: sampleInjectToken
    });

    expect(service.resolve(SomeServiceService).getValue()).toBe('wtf2');
  });

});
