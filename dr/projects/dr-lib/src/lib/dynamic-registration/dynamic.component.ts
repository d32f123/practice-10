import {
  Component,
  ComponentFactoryResolver, ComponentRef,
  InjectionToken,
  Input,
  Type,
  ViewChild
} from '@angular/core';
import {DynamicDirective} from './dynamic.directive';
import {DynamicRegistrationService} from 'dr-lib';


@Component({
  selector: 'lib-dynamic',
  template: `
    <ng-template libDynamic></ng-template>
  `
})
export class DynamicComponent {

  private pToken: Type<any> | InjectionToken<any>;
  @Input()
  public set token(val: Type<any> | InjectionToken<any>) {
    if (this.pToken === val) {
      return;
    }
    this.pToken = val;
    this.loadComponent();
  }

  private pInput: any;
  @Input()
  public set input(val: any) {
    if (this.pInput === val) {
      return;
    }
    this.pInput = val;
    this.setInput();
  }

  private ref: ComponentRef<any>;

  @ViewChild(DynamicDirective, {static: true})
  dynamicHost: DynamicDirective;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private dynamicRegistrationService: DynamicRegistrationService) {
  }

  private loadComponent(): void {
    const component = this.dynamicRegistrationService.getComponent(this.pToken);
    const viewRef = this.dynamicHost.viewContainerRef;
    viewRef.clear();
    if (!component) {
      return;
    }

    const factory = this.componentFactoryResolver.resolveComponentFactory(component);
    this.ref = viewRef.createComponent(factory);
  }

  private setInput(): void {
    if (!this.ref) {
      return;
    }

    this.ref.instance.input = this.pInput;
  }
}
