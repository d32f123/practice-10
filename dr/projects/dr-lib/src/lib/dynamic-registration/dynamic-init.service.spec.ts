import {async, TestBed} from '@angular/core/testing';
import {DynamicRegistrationService, DynamicInitService, SomeServiceService, SomeComponentComponent} from 'dr-lib';

describe('DynamicInitService', () => {
  let service: DynamicInitService;
  let registerService: DynamicRegistrationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SomeComponentComponent],
      providers: [
        {provide: SomeServiceService, useValue: {getValue: () => 'test'}},
        DynamicRegistrationService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(DynamicInitService);
    registerService = TestBed.get(DynamicRegistrationService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('should register component', () => {
    service.init();
    expect(registerService.getComponent(SomeComponentComponent)).toBe(SomeComponentComponent);
  });

  it('should register service', () => {
    service.init();
    expect(registerService.resolve(SomeServiceService).getValue()).toBe('test');
  });

});
