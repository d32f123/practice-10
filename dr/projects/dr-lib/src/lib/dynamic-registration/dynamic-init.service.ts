import {Inject, Injectable, Injector, INJECTOR} from '@angular/core';
import {DynamicRegistrationService} from 'dr-lib';
import {SomeServiceService} from 'dr-lib';
import {SomeComponentComponent} from 'dr-lib';

@Injectable({providedIn: 'root'})
export class DynamicInitService {

  constructor(private dynamicRegistrationService: DynamicRegistrationService,
              @Inject(INJECTOR) private injector: Injector) {
  }

  public init(): void {
    this.dynamicRegistrationService.register(SomeServiceService, {
      token: SomeServiceService,
      injector: this.injector
    });
    this.dynamicRegistrationService.registerComponent(SomeComponentComponent);
  }

}
