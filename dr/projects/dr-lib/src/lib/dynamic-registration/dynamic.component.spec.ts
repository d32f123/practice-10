import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {DynamicRegistrationService} from 'dr-lib';
import {DynamicDirective} from './dynamic.directive';
import {By} from '@angular/platform-browser';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';
import {DynamicComponent} from './dynamic.component';
import {Component, Input, ViewChild} from '@angular/core';


@Component({
  selector: 'lib-dynamic-child',
  template: `
    <p>i am dynamic {{input}}!</p>
  `
})
class DynamicComponentChildComponent {
  @Input()
  public input: string;
}

@Component({
  selector: 'lib-dynamic-component-wrapper',
  template: `
    <lib-dynamic #dynamicComponent [token]="token" [input]="'test'"></lib-dynamic>
  `
})
class DynamicComponentWrapperComponent {
  public token = DynamicComponentChildComponent;

  @ViewChild('dynamicComponent', {static: true})
  public dynamicComponent: DynamicComponent;
}

describe('DynamicComponent', () => {
  let component: DynamicComponent;
  let fixture: ComponentFixture<DynamicComponentWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicComponent, DynamicDirective, DynamicComponentChildComponent, DynamicComponentWrapperComponent],
      providers: [
        DynamicRegistrationService
      ],
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {entryComponents: [DynamicComponentChildComponent]}
      })
      .compileComponents();
  }));

  beforeEach(() => {
    const service: DynamicRegistrationService = TestBed.get(DynamicRegistrationService);
    service.registerComponent(DynamicComponentChildComponent);
    fixture = TestBed.createComponent(DynamicComponentWrapperComponent);
    fixture.detectChanges();
    component = fixture.componentInstance.dynamicComponent;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display dynamic child', () => {
    expect(fixture.debugElement.query(By.css('lib-dynamic-child'))).toBeTruthy();
  });

  it('should set input correctly', () => {
    expect(fixture.debugElement.query(By.css('lib-dynamic-child')).componentInstance.input).toBe('test');
  });

});
