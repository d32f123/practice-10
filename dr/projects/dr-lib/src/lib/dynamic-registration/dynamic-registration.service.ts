import {Injectable, InjectionToken, Injector, Type} from '@angular/core';

class ObjectMap<T> {
  [key: string]: T;
}

export interface RegisterEntry {
  token: Type<any> | InjectionToken<any>;
  injector: Injector;
}

@Injectable({providedIn: 'root'})
export class DynamicRegistrationService {

  private serviceMap = new ObjectMap<RegisterEntry>();
  private componentMap = new ObjectMap<Type<any>>();

  constructor() {
  }

  public register(base: Type<any> | InjectionToken<any>, override: RegisterEntry): void {
    this.serviceMap[base.toString()] = override;
  }

  public registerComponent(base: Type<any> | InjectionToken<any>, component?: Type<any>): void {
    console.log('Registering component: ' + base );
    this.componentMap[base.toString()] = component || (base as Type<any>);
  }

  public resolve<T>(base: Type<T> | InjectionToken<any>): T {
    const entry = this.serviceMap[base.toString()];
    return entry && entry.injector.get(entry.token);
  }

  public getComponent(base: Type<any> | InjectionToken<any>): Type<any> {
    return this.componentMap[base.toString()];
  }

}
