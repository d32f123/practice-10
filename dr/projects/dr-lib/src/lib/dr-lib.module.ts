import { NgModule } from '@angular/core';
import { SomeComponentComponent } from './some-component/some-component.component';
import { HolderComponentComponent } from './holder-component/holder-component.component';
import {DynamicComponent} from './dynamic-registration/dynamic.component';
import {DynamicDirective} from './dynamic-registration/dynamic.directive';

@NgModule({
  declarations: [SomeComponentComponent, HolderComponentComponent, DynamicComponent, DynamicDirective],
  imports: [
  ],
  exports: [SomeComponentComponent, HolderComponentComponent, DynamicComponent],
  entryComponents: [SomeComponentComponent]
})
export class DrLibModule { }
