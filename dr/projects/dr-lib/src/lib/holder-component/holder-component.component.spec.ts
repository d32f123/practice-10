import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HolderComponentComponent } from './holder-component.component';
import {DynamicInitService, DynamicRegistrationService, SomeComponentComponent, SomeServiceService} from 'dr-lib';
import {DynamicDirective} from '../dynamic-registration/dynamic.directive';
import {DynamicComponent} from '../dynamic-registration/dynamic.component';
import {By} from '@angular/platform-browser';
import {BrowserDynamicTestingModule} from '@angular/platform-browser-dynamic/testing';

describe('HolderComponentComponent', () => {
  let component: HolderComponentComponent;
  let fixture: ComponentFixture<HolderComponentComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [HolderComponentComponent, SomeComponentComponent, DynamicComponent, DynamicDirective],
      providers: [
        DynamicInitService,
        SomeServiceService,
        {
          provide: DynamicRegistrationService,
          useValue: {
            getComponent: () => {
              return SomeComponentComponent;
            },
            resolve: () => new SomeServiceService()
          }
        }
      ],
    })
      .overrideModule(BrowserDynamicTestingModule, {
        set: {entryComponents: [SomeComponentComponent]}
      })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HolderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display ', () => {
    expect(fixture.debugElement.query(By.css('lib-some-component'))).toBeTruthy();
  });

});
