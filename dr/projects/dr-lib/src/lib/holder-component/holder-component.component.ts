import { Component, OnInit } from '@angular/core';
import {SomeComponentComponent} from '../some-component/some-component.component';

@Component({
  selector: 'lib-holder-component',
  templateUrl: './holder-component.component.html',
  styleUrls: ['./holder-component.component.css']
})
export class HolderComponentComponent implements OnInit {

  public someComponent = SomeComponentComponent;

  constructor() { }

  ngOnInit() {
  }

}
