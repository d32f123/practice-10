import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class SomeServiceService {
  public getValue(): string {
    return 'Some service';
  }
}
