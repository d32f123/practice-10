import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {DrLibModule} from 'dr-lib';
import {appInitializerFactory, AppInitializerService} from './app-initializer.service';
import { HelloComponent } from './hello/hello.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent
  ],
  imports: [
    BrowserModule,
    DrLibModule
  ],
  providers: [
    AppInitializerService,
    {
      provide: APP_INITIALIZER,
      useFactory: appInitializerFactory,
      deps: [AppInitializerService],
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    HelloComponent
  ]
})
export class AppModule { }
