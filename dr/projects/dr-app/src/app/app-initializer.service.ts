import {Injectable, Injector} from '@angular/core';
import {DynamicInitService, DynamicRegistrationService, SomeComponentComponent, SomeServiceService} from 'dr-lib';
import {HelloComponent} from './hello/hello.component';

export function appInitializerFactory(appInitializerService: AppInitializerService): () => void {
  return () => appInitializerService.execute();
}

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {

  constructor(private dynamicInitService: DynamicInitService,
              private dynamicRegistrationService: DynamicRegistrationService) {
  }

  execute(): void {
    this.enableCustomService();
  }

  enableCustomService(): void {
    this.dynamicInitService.init();
    this.dynamicRegistrationService.register(SomeServiceService, {
      token: SomeServiceService,
      injector: Injector.create({providers: [{provide: SomeServiceService, useValue: {getValue: () => 'that"s now custom!'}}]})
    });
    this.dynamicRegistrationService.registerComponent(SomeComponentComponent, HelloComponent);
  }

}
