import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {DrLibModule} from 'dr-lib';
import {HelloComponent} from './hello/hello.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DrLibModule],
      declarations: [
        AppComponent, HelloComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'dr-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('dr-app');
  });
});
