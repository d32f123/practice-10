import { Component, OnInit } from '@angular/core';
import {DynamicRegistrationService, SomeServiceService} from 'dr-lib';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.less']
})
export class HelloComponent implements OnInit {

  public someService: SomeServiceService;

  constructor(dynamicRegistrationService: DynamicRegistrationService) {
    this.someService = dynamicRegistrationService.resolve(SomeServiceService);
  }

  ngOnInit() {
  }

}
