import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {SmartComponentModule} from 'id-lib';
import {CustomSampleComponent} from './customization/custom-sample/custom-sample.component';
import {register} from '../../../id-lib/src/lib/sample-component/sample-component.component';
import {SampleComponentFacadeImpl} from '../../../id-lib/src/lib/sample-component/facade/sample-component.facade.impl';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SmartComponentModule
      ],
      declarations: [
        AppComponent,
        CustomSampleComponent
      ],
      providers: [SampleComponentFacadeImpl]
    }).compileComponents();
  }));

  beforeEach(() => {
    register();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'id-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('id-app');
  });
});
