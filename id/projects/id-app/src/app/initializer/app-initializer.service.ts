import {Inject, Injectable, Injector, INJECTOR} from '@angular/core';
import {register, SampleComponentFacade} from 'id-lib';
import {SampleComponentCustomFacadeImpl} from '../customization/custom-sample/sample-component.custom.facade.impl';

export function appInitializerFactory(appInitializerService: AppInitializerService): () => Promise<void> {
  return () => appInitializerService.execute();
}

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {

  constructor(@Inject(INJECTOR) public injector: Injector) {
  }

  async execute(): Promise<void> {
    this.enableCustomFacade();
  }

  enableCustomFacade(): void {
    register('SampleComponentFacade', {
      injector: this.injector,
      token: SampleComponentCustomFacadeImpl
    });
  }

}
