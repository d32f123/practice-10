import {async, TestBed} from '@angular/core/testing';
import {AppInitializerService} from './app-initializer.service';
import {SampleComponentCustomFacadeImpl} from '../customization/custom-sample/sample-component.custom.facade.impl';
import {dynamic} from 'id-lib';

describe('AppInitializerService', () => {

  let service: AppInitializerService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [
        SampleComponentCustomFacadeImpl,
        AppInitializerService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    service = TestBed.get(AppInitializerService);
  });

  it('should register custom facade', async(async () => {
    await service.execute();
    expect(dynamic('SampleComponentFacade', null) instanceof SampleComponentCustomFacadeImpl).toBeTruthy();
  }));

});
