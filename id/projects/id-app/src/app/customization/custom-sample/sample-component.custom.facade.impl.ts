import {SampleComponentFacade} from 'id-lib';
import {Injectable} from '@angular/core';

@Injectable()
export class SampleComponentCustomFacadeImpl implements SampleComponentFacade {
  get header(): string {
    return `A CUSTOM facade header`;
  }

  get greeting(): string {
    return `This is a fine custom greeting! Here is some input for you: ${this.sampleInput}`;
  }

  sampleInput: string;
}
