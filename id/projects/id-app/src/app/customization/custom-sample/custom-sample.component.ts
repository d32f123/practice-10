import {Component, Injector, Input, OnInit} from '@angular/core';
import {dynamic, SampleComponentComponent, SampleComponentFacade} from 'id-lib';
import {SampleComponentCustomFacadeImpl} from './sample-component.custom.facade.impl';

export function facadeFactory(injector: Injector) {
  dynamic('SampleComponentFacade', injector);
}
@Component({
  selector: 'app-custom-sample',
  templateUrl: './custom-sample.component.html',
  styleUrls: ['./custom-sample.component.less'],
  providers: [{
    provide: SampleComponentFacade, useClass: SampleComponentCustomFacadeImpl
  }]
})
export class CustomSampleComponent implements OnInit, SampleComponentComponent {
  @Input()
  public set sampleInput(val: string) {
    this.facade.sampleInput = val;
  }

  constructor(public facade: SampleComponentFacade) { }

  ngOnInit() {
  }

}
