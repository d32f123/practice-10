import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomSampleComponent } from './custom-sample.component';

describe('CustomSampleComponent', () => {
  let component: CustomSampleComponent;
  let fixture: ComponentFixture<CustomSampleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomSampleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomSampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
