import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import {SmartComponentModule} from 'id-lib';
import {appInitializerFactory, AppInitializerService} from './initializer/app-initializer.service';
import { CustomSampleComponent } from './customization/custom-sample/custom-sample.component';
import {SampleComponentCustomFacadeImpl} from './customization/custom-sample/sample-component.custom.facade.impl';

@NgModule({
  declarations: [
    AppComponent,
    CustomSampleComponent
  ],
  imports: [
    BrowserModule,
    SmartComponentModule
  ],
  providers: [
    SampleComponentCustomFacadeImpl,
    AppInitializerService,
    {
    provide: APP_INITIALIZER,
    useFactory: appInitializerFactory,
    deps: [AppInitializerService],
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
