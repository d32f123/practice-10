/*
 * Public API Surface of id-lib
 */

export * from './lib/smart-component/smart-component.module';
export * from './lib/smart-component/smart-component.component';
export * from './lib/sample-component/sample-component.module';
export {SampleComponentComponent} from './lib/sample-component/sample-component.component';
export * from './lib/sample-component/facade/sample-component.facade';
export * from './lib/sample-component/facade/sample-component.facade.impl';
export * from './lib/dynamic-registration/dynamic-registration.factory';
export * from './lib/dynamic-registration/dynamic-registration.service';
