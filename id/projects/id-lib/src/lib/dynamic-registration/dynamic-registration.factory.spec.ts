import {InjectionToken, Injector} from '@angular/core';
import {clearStorage, register, registerBase} from './dynamic-registration.service';
import {dynamic} from './dynamic-registration.factory';

describe('RegistrationFactory', () => {

  const otherToken = new InjectionToken('OtherToken');
  const customToken = new InjectionToken('CustomToken');
  const baseInjector = Injector.create([{
    provide: otherToken, useValue: 'base'
  }]);
  const customInjector = Injector.create([{
    provide: customToken, useValue: 'custom'
  }]);

  beforeEach(() => {
    clearStorage();
  });

  it('should throw if no entry found', () => {
    expect(() => dynamic('token', baseInjector)).toThrowError('No storage entry found!');
  });

  it('should resolve base', () => {
    registerBase('token', otherToken);
    expect(dynamic('token', baseInjector)).toBe('base');
  });

  it('should resolve custom', () => {
    registerBase('token', otherToken);
    register('token', {
      token: customToken,
      injector: customInjector
    });
    expect(dynamic('token', baseInjector)).toBe('custom');
  });

});
