import {InjectionToken, Injector, Type} from '@angular/core';

const STORAGE_KEY = '__dynamicStorage';
export interface StorageEntry {
  injector: Injector;
  token: Type<any> | InjectionToken<any>;
}

export function clearStorage(): void {
  delete window[STORAGE_KEY];
}
clearStorage();

function resolveStorage(): any {
  return window[STORAGE_KEY] = window[STORAGE_KEY] || {};
}
export function getStorageEntry(str: string): StorageEntry {
  return resolveStorage()[str];
}
export function setStorageEntry(str: string, entry: StorageEntry): void {
  resolveStorage()[str] = entry;
}

export function registerBase(base: string,
                             baseOverride: any | Type<any> | InjectionToken<any>): void {
  setStorageEntry(base, {
    injector: null,
    token: baseOverride
  });
}

export function register(base: string, override: StorageEntry): void {
  setStorageEntry(base, override);
}
