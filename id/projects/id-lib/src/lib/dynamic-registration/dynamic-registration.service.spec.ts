import {clearStorage, register, registerBase, getStorageEntry, StorageEntry} from './dynamic-registration.service';
import {InjectionToken, Injector} from '@angular/core';

describe('RegistrationService', () => {
  const otherToken = new InjectionToken('OtherToken');
  const injector = Injector.create([]);

  beforeEach(() => {
    clearStorage();
  });

  it('should register base', () => {
    registerBase('token', otherToken);
    expect(getStorageEntry('token')).toEqual({
      injector: null,
      token: otherToken
    } as StorageEntry);
  });

  it('should register', () => {
    register('token', {
      injector,
      token: otherToken
    });
    expect(getStorageEntry('token')).toEqual({
      injector,
      token: otherToken
    });
  });
});
