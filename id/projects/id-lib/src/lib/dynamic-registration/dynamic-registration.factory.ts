import {Injector} from '@angular/core';
import {getStorageEntry} from './dynamic-registration.service';

// If injector == null and baseInjector == null, use baseFactory
export function dynamic<T>(base: string, baseInjector: Injector): any {
  const entry = getStorageEntry(base);
  if (!entry) {
    throw new Error('No storage entry found!');
  }
  if (!entry.injector) {
    return baseInjector.get(entry.token, undefined, undefined);
  }
  return entry.injector.get(entry.token, undefined, undefined);
}
