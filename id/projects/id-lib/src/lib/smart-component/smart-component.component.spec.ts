import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartComponentComponent } from './smart-component.component';
import {SampleComponentComponent} from 'id-lib';
import {By} from '@angular/platform-browser';
import {register} from '../sample-component/sample-component.component';
import {SampleComponentFacadeImpl} from '../sample-component/facade/sample-component.facade.impl';


describe('SmartComponentComponent', () => {
  let component: SmartComponentComponent;
  let fixture: ComponentFixture<SmartComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartComponentComponent, SampleComponentComponent ],
      providers: [SampleComponentFacadeImpl]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    register();
    fixture = TestBed.createComponent(SmartComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set input', () => {
    const sampleComponent: SampleComponentComponent = fixture.debugElement.query(By.css('lib-sample-component')).componentInstance;
    expect(sampleComponent.facade.sampleInput).toBe('some goddamned input');
  });
});
