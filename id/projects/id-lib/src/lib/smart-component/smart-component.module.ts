import {NgModule} from '@angular/core';
import {SmartComponentComponent} from './smart-component.component';
import {SampleComponentModule} from '../sample-component/sample-component.module';

@NgModule({
  imports: [
    SampleComponentModule
  ],
  declarations: [
    SmartComponentComponent
  ],
  exports: [
    SmartComponentComponent
  ]
})
export class SmartComponentModule {}
