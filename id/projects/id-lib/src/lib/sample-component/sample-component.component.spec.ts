import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {register as reg} from 'id-lib';
import {register, SampleComponentComponent} from './sample-component.component';
import {SampleComponentFacadeImpl} from './facade/sample-component.facade.impl';
import {InjectionToken, INJECTOR} from '@angular/core';

describe('SampleComponentComponent', () => {
  let component: SampleComponentComponent;
  let fixture: ComponentFixture<SampleComponentComponent>;
  const token = new InjectionToken('someotkne');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SampleComponentComponent ],
      providers: [SampleComponentFacadeImpl, {
        provide: token,
        useValue: {
          header: 'test',
          greeting: 'test'
        }
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    register();
    fixture = TestBed.createComponent(SampleComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have facade', () => {
    expect(component.facade).toBeTruthy();
  });

  it('should have custom facade', () => {
    reg('SampleComponentFacade', {
      token,
      injector: TestBed.get(INJECTOR)
    });
    expect(TestBed.createComponent(SampleComponentComponent).componentInstance.facade.greeting).toBe('test');
  });
});
