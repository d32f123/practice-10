import {Component, INJECTOR, Injector, Input, OnInit} from '@angular/core';
import {SampleComponentFacade} from './facade/sample-component.facade';
import {dynamic} from '../dynamic-registration/dynamic-registration.factory';
import {registerBase} from '../dynamic-registration/dynamic-registration.service';
import {SampleComponentFacadeImpl} from './facade/sample-component.facade.impl';

export const register = () => registerBase('SampleComponentFacade', SampleComponentFacadeImpl);
register();
export function facadeFactory(injector: Injector) {
  console.log('factory called');
  return dynamic('SampleComponentFacade', injector);
}
@Component({
  selector: 'lib-sample-component',
  templateUrl: './sample-component.component.html',
  styleUrls: ['./sample-component.component.css'],
  providers: [SampleComponentFacadeImpl, {
    provide: SampleComponentFacade, useFactory: facadeFactory, deps: [INJECTOR]
  }]
})
export class SampleComponentComponent implements OnInit {

  @Input()
  public set sampleInput(val: string) {
    this.facade.sampleInput = val;
  }

  constructor(public facade: SampleComponentFacade) { }

  ngOnInit() {
  }

}
