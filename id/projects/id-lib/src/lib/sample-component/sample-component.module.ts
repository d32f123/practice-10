import {NgModule} from '@angular/core';
import {SampleComponentComponent} from './sample-component.component';

@NgModule({
  declarations: [
    SampleComponentComponent
  ],
  exports: [
    SampleComponentComponent
  ]
})
export class SampleComponentModule {}
