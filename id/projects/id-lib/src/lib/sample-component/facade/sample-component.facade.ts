
export abstract class SampleComponentFacade {
  public readonly header: string;
  public readonly greeting: string;

  public sampleInput: string;
}
