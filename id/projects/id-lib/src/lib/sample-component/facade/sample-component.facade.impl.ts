import {Injectable} from '@angular/core';
import {SampleComponentFacade} from './sample-component.facade';

@Injectable()
export class SampleComponentFacadeImpl extends SampleComponentFacade {

  public get header(): string {
    return `Library sample component`;
  }

  public get greeting(): string {
    return `A hello from library facade! Input was: ${this.sampleInput}`;
  }

  sampleInput: string;
}
