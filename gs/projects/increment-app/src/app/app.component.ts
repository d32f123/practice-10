import { Component } from '@angular/core';
import {GlobalStateService} from 'gs-lib';

@Component({
  selector: 'app-increment-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'increment-app';

  constructor(public globalStateService: GlobalStateService) {
  }

}
