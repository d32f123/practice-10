import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { DecrementAppModule } from 'decrement-app';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    DecrementAppModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
