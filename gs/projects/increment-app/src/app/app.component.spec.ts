import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {DecrementAppComponent, DecrementAppModule} from 'decrement-app';
import {By} from '@angular/platform-browser';
import {clearStorage} from 'gs-lib';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [DecrementAppModule],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'increment-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('increment-app');
  });

  it('should have service resolved', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    expect(app.globalStateService).toBeTruthy();
  });

  it ('should have service actual', () => {
    clearStorage();
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    const state = app.globalStateService.getState();
    expect(state).toBe(0);
    app.globalStateService.increment();
    expect(app.globalStateService.getState()).toBe(state + 1);
  });

  it('decrement app should affect increment app', () => {
    clearStorage();
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    const decrementApp: DecrementAppComponent = fixture.debugElement.query(By.css('lib-decrement-app')).componentInstance;
    expect(app.globalStateService.getState()).toBe(0);
    expect(decrementApp.globalStateService.getState()).toBe(0);
    app.globalStateService.increment();
    expect(app.globalStateService.getState()).toBe(1);
    expect(decrementApp.globalStateService.getState()).toBe(1);
    decrementApp.globalStateService.decrement();
    decrementApp.globalStateService.decrement();
    expect(app.globalStateService.getState()).toBe(-1);
    expect(decrementApp.globalStateService.getState()).toBe(-1);
  });
});
