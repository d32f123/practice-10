import {inject, Type} from '@angular/core';

const STORAGE_KEY = '__globalServices';

export function share<T>(name: string, clsType: Type<T>, ...deps: any[]): T {
  const storage = window[STORAGE_KEY] = window[STORAGE_KEY] || {};
  if (!!storage[name]) {
    return storage[name];
  }
  const cls: new(...deps: any[]) => T = (clsType as any)();
  storage[name] = new cls(...deps.map(dep => inject(dep)));
  return storage[name];
}

export function clearStorage(): void {
  delete window[STORAGE_KEY];
}
