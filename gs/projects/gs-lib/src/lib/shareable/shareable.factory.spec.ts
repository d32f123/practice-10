import {share, clearStorage} from './shareable.factory';
import {forwardRef} from '@angular/core';

class TestClass {
}

describe('ShareableFactory', () => {
  const factory = () => share('TestClass', forwardRef(() => TestClass));

  it('should return same class', () => {
    expect(factory()).toBe(factory());
  });

  it('should persist class in window', () => {
    clearStorage();
    factory();
    expect((window as any).__globalServices.TestClass).toBe(factory());
  });

});
