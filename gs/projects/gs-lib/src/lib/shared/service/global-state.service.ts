import {forwardRef, Injectable} from '@angular/core';
import {SomeDepService} from './some-dep.service';
import {share} from '../../shareable/shareable.factory';


export function factory() {
  return share('GlobalStateService', forwardRef(() => GlobalStateService), SomeDepService);
}

@Injectable({
  providedIn: 'root',
  useFactory: factory
})
export class GlobalStateService {
  constructor(public someDepService: SomeDepService) {
  }

  private state = 0;

  public getState(): number {
    return this.state;
  }

  public increment(): void {
    this.state++;
  }

  public decrement(): void {
    this.state--;
  }

}
