import {async, TestBed} from '@angular/core/testing';
import {GlobalStateService} from './global-state.service';
import {clearStorage} from '../../shareable/shareable.factory';

describe('GlobalStateService', () => {
  let service: GlobalStateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
    })
      .compileComponents();
  }));

  beforeEach(() => {
    clearStorage();
    service = TestBed.get(GlobalStateService);
  });

  it('should create', () => {
    expect(service).toBeTruthy();
  });

  it('initially should have state 0', () => {
    expect(service.getState()).toBe(0);
  });

  it('should decrement', () => {
    service.decrement();
    expect(service.getState()).toBe(-1);
  });

  it('should increment', () => {
    service.increment();
    expect(service.getState()).toBe(1);
  });

  it('should return same service', () => {
    const anotherService = TestBed.get(GlobalStateService);
    expect(service).toBe(anotherService);
  });
});
