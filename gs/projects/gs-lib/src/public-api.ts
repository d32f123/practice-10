/*
 * Public API Surface of gs-lib
 */

export { GlobalStateService } from './lib/shared/service/global-state.service';
export * from './lib/shareable/shareable.factory';
