import { Component, OnInit } from '@angular/core';
import {GlobalStateService} from 'gs-lib';

@Component({
  selector: 'lib-decrement-app',
  templateUrl: './decrement-app.component.html',
  styles: []
})
export class DecrementAppComponent implements OnInit {

  constructor(public globalStateService: GlobalStateService) { }

  ngOnInit() {
  }

}
