import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DecrementAppComponent } from './decrement-app.component';
import {clearStorage} from 'gs-lib';

describe('DecrementAppComponent', () => {
  let component: DecrementAppComponent;
  let fixture: ComponentFixture<DecrementAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DecrementAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DecrementAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have service resolved', () => {
    expect(component.globalStateService).toBeTruthy();
  });

  it ('should have service actual', () => {
    clearStorage();
    const state = component.globalStateService.getState();
    expect(state).toBe(0);
    component.globalStateService.decrement();
    expect(component.globalStateService.getState()).toBe(state - 1);
  });
});
