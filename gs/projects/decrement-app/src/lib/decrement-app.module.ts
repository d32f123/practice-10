import { NgModule } from '@angular/core';
import { DecrementAppComponent } from './decrement-app.component';



@NgModule({
  declarations: [DecrementAppComponent],
  imports: [
  ],
  exports: [DecrementAppComponent]
})
export class DecrementAppModule { }
